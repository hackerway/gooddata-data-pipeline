stages:
  - build
  - extract_load
  - transform
  - analytics

##########################
# Global variables
variables:
  SRC_DATA_PIPELINE: "data_pipeline"
  DBT_PROFILE_DIR: "profile"
  DBT_PROFILE: "default"
  GOODDATA_HOST: "https://demo-cicd.cloud.gooddata.com"
  SNOWFLAKE_ACCOUNT: "gooddata"
  SNOWFLAKE_USER: "cicd"
  SNOWFLAKE_WAREHOUSE: "DEMO_WH"
  VERTICA_HOST: "140.236.88.151"
  VERTICA_PORT: "5433"
  VERTICA_USER: "gooddata"
  VERTICA_DBNAME: "PartPub80DB"
  INPUT_SCHEMA_FAA: "faa_input_stage"
  INPUT_SCHEMA_GITHUB: "github_input_stage"
  INPUT_SCHEMA_EXCHANGERATEHOST: "exchangeratehost_input_stage"
  OUTPUT_SCHEMA: "cicd_output_stage"
  IMAGES_WORKDIR: "/project"
  MELTANO_CUSTOM_IMAGE_BASE: "gooddata-data-pipeline-meltano"
  MELTANO_VERSION: "v2.16.0-python3.10"
  MELTANO_CUSTOM_IMAGE: "$CI_REGISTRY_IMAGE/$MELTANO_CUSTOM_IMAGE_BASE:$MELTANO_VERSION"
  MELTANO_STATE_AWS_BUCKET: "gdc-quiver"
  DBT_CUSTOM_IMAGE_BASE: "gooddata-data-pipeline-dbt"
  DBT_VERSION: "1.4.1"
  DBT_CUSTOM_IMAGE: "$CI_REGISTRY_IMAGE/$DBT_CUSTOM_IMAGE_BASE:$DBT_VERSION"
  PYTHON_IMAGE: "python:3.10.11-slim-bullseye"
  DBT_GOODDATA_IMAGE_BASE: "gooddata-data-pipeline-dbt-gooddata"
  DBT_GOODDATA_VERSION: "0.3.8"
  DBT_GOODDATA_IMAGE: "$CI_REGISTRY_IMAGE/$DBT_GOODDATA_IMAGE_BASE:$DBT_GOODDATA_VERSION"

.envs:
  # Meltano, dbt and GoodData environments have 1:1 relationship in this demo
  # But, you can design you pipeline in any alternative way, e.g. share 1 data source by multiple GoodData workspaces
  dev:
    ELT_ENVIRONMENT: "cicd_dev"
    SNOWFLAKE_DBNAME: "CICD_DEV"
    GOODDATA_ENVIRONMENT_ID: "development"
  staging:
    ELT_ENVIRONMENT: "cicd_staging"
    SNOWFLAKE_DBNAME: "CICD_STAGING"
    GOODDATA_ENVIRONMENT_ID: "staging"
    ELT_ENVIRONMENT_VERTICA: "cicd_staging_vertica"
    GOODDATA_ENVIRONMENT_ID_VERTICA: "staging_vertica"
  prod:
    ELT_ENVIRONMENT: "cicd_prod"
    SNOWFLAKE_DBNAME: "CICD_PROD"
    GOODDATA_ENVIRONMENT_ID: "production"

  # alternatively define environments for Cloud versions – dbt Cloud and Meltano Cloud
  cloud_dev:
    ELT_ENVIRONMENT: "cicd_cloud_dev"
    SNOWFLAKE_DBNAME: "CICD_CLOUD_DEV"
    DBT_JOB_ID: 406899
    GOODDATA_ENVIRONMENT_ID: "cloud_development"
  cloud_staging:
    ELT_ENVIRONMENT: "cicd_cloud_staging"
    SNOWFLAKE_DBNAME: "CICD_CLOUD_STAGING"
    GOODDATA_ENVIRONMENT_ID: "cloud_staging"
    ELT_ENVIRONMENT_VERTICA: "cicd_cloud_staging_vertica"
    GOODDATA_ENVIRONMENT_ID_VERTICA: "cloud_staging_vertica"
    DBT_JOB_ID: 408385
  cloud_prod:
    ELT_ENVIRONMENT: "cicd_cloud_prod"
    SNOWFLAKE_DBNAME: "CICD_CLOUD_PROD"
    GOODDATA_ENVIRONMENT_ID: "cloud_production"
    DBT_JOB_ID: 408386



##########################
# Job templates
##########################
.base:
  image: python:3.10-slim-bullseye

.base_rules:
  rules:
    - if: '$RUN_ALL_JOBS == "true"'
      when: manual

.elt_rules:
  rules:
    - if: '$FULL_REFRESH == "true"'
      when: manual

# Separate files for each use case
# Prevent pipeline to be running if we change one gitlab-ci.yaml file containing all use cases
include:
  - ".gitlab-ci/gitlab-ci-build-base.yml"
  - ".gitlab-ci/gitlab-ci-build-meltano.yml"
  - ".gitlab-ci/gitlab-ci-build-dbt.yml"
  - ".gitlab-ci/gitlab-ci-build-dbt-gooddata.yml"
  - ".gitlab-ci/gitlab-ci-extract-load.yml"
  - ".gitlab-ci/gitlab-ci-transform.yml"
  - ".gitlab-ci/gitlab-ci-analytics.yml"
