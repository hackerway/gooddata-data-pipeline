#!/bin/env bash

if [ -f "./.env.custom.local" ]; then
  source ./.env.custom.local
fi

if [ "$1" == "vertica" ]; then
  export MELTANO_TARGET="target-vertica"
  export ELT_ENVIRONMENT="cicd_dev_local_vertica"
else
  export MELTANO_TARGET="target-postgres"
  export ELT_ENVIRONMENT="cicd_dev_local"
fi

# Shared DB params
export POSTGRES_HOST="localhost"
export POSTGRES_PORT=5432
export POSTGRES_USER="demouser"
export POSTGRES_PASS=demopass
export POSTGRES_DBNAME=demo
export VERTICA_HOST="localhost"
export VERTICA_PORT=5433
export VERTICA_USER="demouser"
export VERTICA_PASS=demopass
export VERTICA_DBNAME=vmart
export INPUT_SCHEMA_GITHUB="github_input_stage"
export INPUT_SCHEMA_FAA="faa_input_stage"
export INPUT_SCHEMA_EXCHANGERATEHOST="exchangeratehost_input_stage"
export OUTPUT_SCHEMA="cicd_output_stage"

# Meltano
export MELTANO_STATE_AWS_ACCESS_KEY_ID="minio_abcde_k1234567"
export MELTANO_STATE_AWS_SECRET_ACCESS_KEY="minio_abcde_k1234567_secret1234567890123"
export MELTANO_STATE_AWS_BUCKET="meltano"
export MELTANO_STATE_AWS_ENDPOINT="http://localhost:19000"

# dbt
export DBT_PROFILE_DIR="profile"
export DBT_PROFILE="default"

# dbt env var needed for dbt Cloud
for var in $(env | grep -E 'POSTGRES_|VERTICA_|_SCHEMA' | cut -d= -f1); do
    # Remove the "POSTGRES_" prefix and add "DBT_" prefix
    new_var="DBT_${var}"
    # Get the value of the original variable
    value="${!var}"
    # Set the new variable with the modified value
    export "$new_var=$value"
done

# GoodData
export GOODDATA_HOST="http://localhost:3000"
export GOODDATA_ENVIRONMENT_ID="development"
unset GOODDATA_UPPER_CASE
export GOODDATA_TOKEN="YWRtaW46Ym9vdHN0cmFwOmFkbWluMTIz"
